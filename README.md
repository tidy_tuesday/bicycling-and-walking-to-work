# Tidy Tuesday - Week #45
### Bicycling and Walking to Work

This is my homage to @PaulCampbell91's post, [MULTIVARIATE DOT-DENSITY MAPS IN R WITH SF & GGPLOT2](https://www.cultureofinsight.com/post/multivariate-dot-density-maps-in-r-with-sf-ggplot2), from last year. I wanted to highlight the split between those commuting by bike and by walking and thought of this chart. Please check out his blog post for a much better walkthrough of the code. 

![](BikeOrWalk.png)

## Code Snippet
```r
library(tidyverse)
library(usmap)
library(maps)
library(sf)

my_font <- "Comfortaa"

commute_mode <- readr::read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-11-05/commute.csv")

states <- st_as_sf(map("state", plot = FALSE, fill = TRUE))
head(states)
states <- cbind(states, st_coordinates(st_centroid(states)))
states$ID <- str_to_title(states$ID)

# clean up
df1$state[!df1$state %in% states$ID]
df1$state[df1$state=="Ca"] <- "California"
df1$state[df1$state=="Massachusett"] <- "Massachusetts"
df1$state[df1$state=="District of Columbia"] <- "District Of Columbia"

df2 <- df1 %>%
  group_by(state) %>%
  mutate(st_sum = sum(n)) %>%
  group_by(state, mode) %>%
  summarize(mode_sum = sum(n)) %>%
  arrange(desc(state),mode)

df3 <- df2 %>%
  left_join(states, by = c("state" = "ID")) %>%
  spread(key=mode, value = mode_sum) %>% 
  filter(!state %in% c("Alaska","Hawaii")) %>%    # Alaska and Hawaii were not in the dataset -- removing 
  st_as_sf()                                      # them here prevents an error
  
# credit to Jens von Bergmann for this algo https://github.com/mountainMath/dotdensity/blob/master/R/dot-density.R
random_round <- function(x) {
    v=as.integer(x)
    r=x-v
    test=runif(length(r), 0.0, 1.0)
    add=rep(as.integer(0),length(r))
    add[r>test] <- as.integer(1)
    value=v+add
    ifelse(is.na(value) | value<0,0,value)
    return(value)
  }

dotnum <- 250

# data frame of number of dots to plot for each mode (1 for every 100 votes)
num_dots <- as.data.frame(df3) %>% 
  select(Bike:Walk) %>% 
  mutate_all(funs(. / dotnum)) %>% 
  mutate_all(random_round)

# generates data frame with coordinates for each point + what mode it is assiciated with
sf_dots <- map_df(names(num_dots), 
                  ~ st_sample(df3, size = num_dots[,.x], type = "random") %>%     # generate the points in each polygon
                    st_cast("POINT") %>%                                          # cast the geom set as 'POINT' data
                    st_coordinates() %>%                                          # pull out coordinates into a matrix
                    as_tibble() %>%                                               # convert to tibble
                    setNames(c("lon","lat")) %>%                                  # set column names
                    mutate(Mode = .x)                                             # add categorical mode variable
                  ) %>% 
  slice(sample(1:n())) # once map_df binds rows randomise order to avoid bias in plotting order

# colour palette for modes
pal <- c("Bike" = "#FCFDBFFF", "Walk" = "#B63679FF")

# plot it and save as png big enough to avoid over-plotting of the points
p <- ggplot() +
  geom_sf(data = df3, fill = "transparent",colour = "white") +
  geom_point(data = sf_dots, aes(lon, lat, colour = Mode)) +
  scale_colour_manual(values = pal) +
  coord_sf(crs = 4326, datum = NA) +
  #theme_void(base_family = "Iosevka", base_size = 48) +
  theme_void(base_family = my_font, base_size = 48) +
  labs(x = NULL, y = NULL,
       title = "Modes Less Traveled - Bicycling and Walking to Work\nin the United States: 2008-2012\n",
       subtitle = paste0("Commuters\n1 dot = ",dotnum," people"),
       caption = "By @DaveBloom11 (Copied from @PaulCampbell91) | Data Sources: U.S. Census Bureau, American Community Survey") +
  guides(colour = guide_legend(override.aes = list(size = 18))) +
  theme(legend.position = c(0.82, 1.03), legend.direction = "horizontal",
        plot.background = element_rect(fill = "#212121", color = NA), 
        panel.background = element_rect(fill = "#212121", color = NA),
        legend.background = element_rect(fill = "#212121", color = NA),
        legend.key = element_rect(fill = "#212121", colour = NA),
        plot.margin = margin(1, 1, 1, 1, "cm"),
        text =  element_text(color = "white"),
        title =  element_text(color = "white"),
        plot.title = element_text(hjust = 0.5),
        plot.caption = element_text(size = 32)
  )

ggsave("BikeOrWalk.png", plot = p, dpi = 320, width = 80, height = 57, units = "cm")


```
